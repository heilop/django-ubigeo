======
Ubigeo
======

Esta app contiene modelos de los Ubigeos de Perú.

Contiene la base da datos de los ubigeos de INEI, RENIEC y SUNAT.

Quick start
-----------

1. Add "ubigeo" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'ubigeo',
    ]

